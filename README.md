#Noise study
This project is used to explore signal to noise properties.  

## Building
mvn clean install

## Projects
**Median Blending**  
1. Create bitmap images with gaussian (bell curve) shaped random white noise.  
2. On each image add a signal.  For example, some text with a constant or near constant pixel value.  
3. Stack the images using a median blend operation on each pixel.

The larger the number of frames the more pronounced the signal should be.

