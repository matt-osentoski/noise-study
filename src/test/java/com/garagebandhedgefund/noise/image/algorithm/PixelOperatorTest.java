package com.garagebandhedgefund.noise.image.algorithm;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Matt on 4/2/2016.
 */
public class PixelOperatorTest {

    private int[] evenPixels = {1, 2, 3, 4, 5, 6, 7, 8};
    private int[] oddPixels = {1, 2, 3, 4, 5, 6, 7};
    // Mix up the array order
    private int[] evenPixelsMixed = {1, 4, 7, 2, 5, 6, 3, 8};
    private int[] oddPixelsMixed = {1, 6, 5, 4, 3, 2, 7};

    @Test
    public void medianPixel() throws Exception {

        int evenPixel = PixelOperator.medianPixel(evenPixels);
        int oddPixel = PixelOperator.medianPixel(oddPixels);
        assertEquals(4, oddPixel);
        assertEquals(4, evenPixel);
        // Negative testing
        assertNotEquals(2, oddPixel);
        assertNotEquals(5, evenPixel);

        // Test Mixed array order
        int evenPixelMixed = PixelOperator.medianPixel(evenPixelsMixed);
        int oddPixelMixed = PixelOperator.medianPixel(oddPixelsMixed);
        assertEquals(4, oddPixelMixed);
        assertEquals(4, evenPixelMixed);
        // Negative testing
        assertNotEquals(2, oddPixelMixed);
        assertNotEquals(5, evenPixelMixed);
    }

    @Test
    public void pixelByPercentChange() throws Exception {
        int pixelValue = PixelOperator.pixelByPercentChange(0.03, 256, 0.05, 0.5);
        assertEquals(164, pixelValue);

        pixelValue = PixelOperator.pixelByPercentChange(-0.03, 256, 0.05, 0.5);
        assertEquals(90, pixelValue);

        pixelValue = PixelOperator.pixelByPercentChange(0.06, 256, 0.05, 0.5);
        assertEquals(191, pixelValue);

        pixelValue = PixelOperator.pixelByPercentChange(-0.06, 256, 0.05, 0.5);
        assertEquals(63, pixelValue);
    }

}