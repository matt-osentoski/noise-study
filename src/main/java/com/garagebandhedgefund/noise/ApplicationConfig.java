package com.garagebandhedgefund.noise;

import com.garagebandhedgefund.noise.image.ImageCreator;
import com.garagebandhedgefund.noise.image.StockPriceImageCreatorImpl;
import com.garagebandhedgefund.noise.image.builder.ChaosImage;
import com.garagebandhedgefund.noise.image.builder.ImageFrame;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Random;
import java.util.function.Function;

/**
 * Created by Matt on 4/2/2016.
 */
@Configuration
public class ApplicationConfig {

    @Bean
    public ImageFrame chaosImage() {
        ImageFrame image = new ChaosImage(new Random());
        return image;
    }

    /**
     * This is just a placeholder to allow for dynamic wiring of the stockPriceImageCreator, below.
     * @return
     */
    @Bean
    public String startDate() {
        return null;
    }

    /**
     * This Bean is a factory that allows stockPriceImageCreator below to be given a
     * dynamic runtime constructor value.
     * @return
     */
    @Bean
    public Function<String, ImageCreator> stockPriceImageCreatorFactory() {
        return startDate -> stockPriceImageCreator(startDate); // or this::thing
    }

    @Bean
    @Scope(value = "prototype")
    public ImageCreator stockPriceImageCreator(String startDate) {
        return new StockPriceImageCreatorImpl(startDate);
    }
}
