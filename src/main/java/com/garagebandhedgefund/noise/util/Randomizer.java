package com.garagebandhedgefund.noise.util;

import java.util.Random;

/**
 * Created by Matt on 4/1/2016.
 */
public class Randomizer {

    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param random A random object
     * @param min Minimum value
     * @param max Maximum value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    public static int randomRange(Random random, int min, int max) {

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = random.nextInt((max - min) + 1) + min;
        //System.out.println("rand:" + randomNum);
        return randomNum;
    }
}
