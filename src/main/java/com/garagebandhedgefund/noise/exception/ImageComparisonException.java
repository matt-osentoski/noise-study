package com.garagebandhedgefund.noise.exception;

/**
 * Created by Matt on 4/2/2016.
 */
public class ImageComparisonException extends Exception {
    public ImageComparisonException() { super(); }
    public ImageComparisonException(String message) { super(message); }
    public ImageComparisonException(String message, Throwable cause) { super(message, cause); }
    public ImageComparisonException(Throwable cause) { super(cause); }
}
