package com.garagebandhedgefund.noise.data;

import com.garagebandhedgefund.trading.model.StockPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Matt on 6/25/2017.
 */
@Component
public class StockPriceRepositoryImpl implements StockPriceCustomRepository {

    @Autowired
    private StockPriceRepository stockPriceRepository;

    @Override
    public Map<String, StockPrice> getMappedStockPricesByDate(Date priceDate) {
        List<StockPrice> stockPrices = stockPriceRepository.getStockPricesByDate(priceDate);
        Map<String, StockPrice> stockPriceMap = new HashMap<String, StockPrice>();

        for (StockPrice stockPrice: stockPrices) {
            stockPriceMap.put(stockPrice.getSymbol(), stockPrice);
        }
        return stockPriceMap;
    }
}
