package com.garagebandhedgefund.noise.data;

import com.garagebandhedgefund.trading.model.StockPrice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * Created by Matt on 5/1/2016.
 */
public interface StockPriceRepository extends CrudRepository<StockPrice, Long> {

    // SELECT DISTINCT price_date FROM stock_prices WHERE price_date >= DATE('2015-01-01') ORDER BY price_date
    @Query("SELECT DISTINCT s.priceDate FROM StockPrice s WHERE s.priceDate > :startDate ORDER BY s.priceDate")
    List<Date> getStockDates(@Param("startDate")Date startDate);

    // SELECT * FROM stock_prices s WHERE price_date = DATE('2015-10-02')
    @Query("SELECT s FROM StockPrice s WHERE s.priceDate = :priceDate")
    List<StockPrice> getStockPricesByDate(@Param("priceDate")Date priceDate);
}
