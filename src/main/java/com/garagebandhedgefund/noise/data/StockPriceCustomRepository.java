package com.garagebandhedgefund.noise.data;

import com.garagebandhedgefund.trading.model.StockPrice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Matt on 5/1/2016.
 */
public interface StockPriceCustomRepository {
    Map<String, StockPrice> getMappedStockPricesByDate(Date priceDate);
}
