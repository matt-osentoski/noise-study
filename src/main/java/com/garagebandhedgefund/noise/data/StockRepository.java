package com.garagebandhedgefund.noise.data;

import com.garagebandhedgefund.trading.model.Stock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Matt on 5/1/2016.
 */
public interface StockRepository extends CrudRepository<Stock, Long> {

    // SELECT * FROM stocks s where market_cap > 67000000000 order by sector, market_cap desc LIMIT 100
    @Query("SELECT s FROM Stock s WHERE s.marketCap > 67000000000 ORDER BY s.sector, s.marketCap DESC")
    List<Stock> getTopStocks();


}
