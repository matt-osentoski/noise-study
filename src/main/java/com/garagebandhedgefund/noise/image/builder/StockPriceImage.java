package com.garagebandhedgefund.noise.image.builder;

import com.garagebandhedgefund.noise.image.algorithm.PixelOperator;
import com.garagebandhedgefund.trading.formulas.PriceChangeFormulas;
import com.garagebandhedgefund.trading.model.Stock;
import com.garagebandhedgefund.trading.model.StockPrice;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Matt on 5/1/2016.
 */
public class StockPriceImage implements ImageFrame  {

    private List<Stock> topStocks;
    private Map<String, StockPrice> previousPrices;
    private Map<String, StockPrice> currentPrices;

    public StockPriceImage(List<Stock> topStocks, Map<String, StockPrice> previousPrices,
                           Map<String, StockPrice> currentPrices) {
        this.topStocks = topStocks;
        this.previousPrices = previousPrices;
        this.currentPrices = currentPrices;
    }

    @Override
    public BufferedImage createImage(int width, int height) throws IOException {
        BufferedImage bImg = new BufferedImage(width,height, BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster raster = bImg.getRaster();
        // Put the pixels on the raster, using values between 0 and 255.
        int stockIdx = 0;
        for(int h=0;h<height;h++) {
            for (int w = 0; w < width; w++) {
                if (stockIdx > topStocks.size() -1) {
                    break;
                }
                Stock stock = topStocks.get(stockIdx);
                StockPrice previousPrice = previousPrices.get(stock.getSymbol());
                StockPrice currentPrice = currentPrices.get(stock.getSymbol());
                if (stock == null || previousPrice == null || currentPrice == null) {
                    raster.setSample(w, h, 0, 0);
                } else {
                    double change = PriceChangeFormulas.percentChange(previousPrice.getClosingPrice(),
                            currentPrice.getClosingPrice(), 2) / 100;
                    int value = PixelOperator.pixelByPercentChange(change, 256, 0.03, 0.75);
                    raster.setSample(w, h, 0, value);
                }
                stockIdx++;
            }
        }
        return bImg;
    }
}
