package com.garagebandhedgefund.noise.image.builder;

import com.garagebandhedgefund.noise.util.Randomizer;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Matt on 4/1/2016.
 */
@Component
public class ChaosImage implements ImageFrame {

    private Random random;

    public ChaosImage(Random random) {
        this.random = random;
    }

    /**
     * Creates an image with random pixel values
     * @param width Width of the image
     * @param height Height of the image
     * @return BufferedImage
     * @throws IOException
     */
    @Override
    public BufferedImage createImage(int width, int height) throws IOException {
        BufferedImage bImg = new BufferedImage(width,height, BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster raster = bImg.getRaster();
        // Put the pixels on the raster, using values between 0 and 255.
        for(int h=0;h<height;h++) {
            for (int w = 0; w < width; w++) {
                int value = Randomizer.randomRange(random, 0, 255);
                raster.setSample(w, h, 0, value);
            }
        }
        return bImg;
    }
}
