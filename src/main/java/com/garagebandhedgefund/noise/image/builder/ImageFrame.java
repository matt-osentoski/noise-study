package com.garagebandhedgefund.noise.image.builder;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by Matt on 4/3/2016.
 */
public interface ImageFrame {
    BufferedImage createImage(int width, int height) throws IOException;
}
