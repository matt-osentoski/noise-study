package com.garagebandhedgefund.noise.image;

import com.garagebandhedgefund.noise.data.StockPriceCustomRepository;
import com.garagebandhedgefund.noise.data.StockPriceRepository;
import com.garagebandhedgefund.noise.data.StockPriceRepositoryImpl;
import com.garagebandhedgefund.noise.data.StockRepository;
import com.garagebandhedgefund.noise.image.builder.ImageFrame;
import com.garagebandhedgefund.noise.image.builder.StockPriceImage;
import com.garagebandhedgefund.trading.model.Stock;
import com.garagebandhedgefund.trading.model.StockPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Matt on 5/1/2016.
 */
public class StockPriceImageCreatorImpl implements ImageCreator {

    private String stockPriceStartDateStr;

    @Autowired
    StockRepository stockRepository;

    @Autowired
    StockPriceRepository stockPriceRepository;

    @Autowired
    StockPriceCustomRepository stockPriceRepositoryImpl;

    @Value("${image.frames}")
    int imageFrames;

    public StockPriceImageCreatorImpl(String startDate) {
        this.stockPriceStartDateStr = startDate;
    }

    @Override
    public List<BufferedImage> createImages(String fileLocation, int width, int height) throws IOException {
        List<BufferedImage> images = new ArrayList<BufferedImage>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = sdf.parse(stockPriceStartDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // # Find the top 100 stocks
        List<Stock> topStocks = stockRepository.getTopStocks();

        // # Find all stock dates
        List<Date> stockDates = stockPriceRepository.getStockDates(startDate);

        // # Find stocks for a particular date
        Map<String, StockPrice> previousPrices = stockPriceRepositoryImpl.getMappedStockPricesByDate(stockDates.get(0));
        for (int x=1; x< stockDates.size() -1; x++) {
            if (x > imageFrames) {
                break;
            }
            Map<String, StockPrice> currentPrices =
                    stockPriceRepositoryImpl.getMappedStockPricesByDate(stockDates.get(x));
            ImageFrame image = new StockPriceImage(topStocks, previousPrices,currentPrices);
            File file = new File(fileLocation +"/" + stockDates.get(x).getTime() + "-" + x + ".png");
            BufferedImage bImage = image.createImage(width, height);
            ImageIO.write(bImage, "png", file);
            images.add(bImage);
            previousPrices = currentPrices;
        }
        return images;
    }
}
