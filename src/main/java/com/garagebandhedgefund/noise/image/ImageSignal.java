package com.garagebandhedgefund.noise.image;

import org.springframework.stereotype.*;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The class is used to create a signal within an image.<br>
 *<br>
 * For example, to create some white text within an image filled with Gaussian white noise.  The text can then be
 * parsed out when enough images are stacked since the signal will remain constant while the random noise changes.
 *
 * Created by Matt on 4/2/2016.
 */
@Component
public class ImageSignal {

    /**
     * Places text in a noisy image.
     * @param randomImage An image with random noise as the background
     * @param textSignal Text that will be used as a signal
     * @param x The X location where the text will start
     * @param y The Y location where the text will start
     */
    public void writeTextSignal(BufferedImage randomImage, String textSignal, int x, int y) {
        Graphics2D g2d = randomImage.createGraphics();
        g2d.setPaint(Color.WHITE);
        g2d.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 8));
        FontMetrics fm = g2d.getFontMetrics();
        g2d.drawString(textSignal, x, y);
        g2d.dispose();
    }
}
