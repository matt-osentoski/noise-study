package com.garagebandhedgefund.noise.image;

import com.garagebandhedgefund.noise.image.builder.ImageFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matt on 5/1/2016.
 */
@Component
public class RandomImageCreatorImpl implements ImageCreator {

    @Autowired
    ImageFrame chaosImage;

    @Autowired
    ImageSignal imageSignal;

    @Value("${image.frames}")
    int imageFrames;

    @Override
    public List<BufferedImage> createImages(String fileLocation, int width, int height) throws IOException {
        List<BufferedImage> randomImages = new ArrayList<BufferedImage>();
        for (int x = 0; x<imageFrames; x++) {
            File file = new File(fileLocation +"/" + "random-" + x + ".png");
            BufferedImage randomImage =chaosImage.createImage(width, height);
            imageSignal.writeTextSignal(randomImage, ":)", 0, 6);
            ImageIO.write(randomImage, "png", file);
            randomImages.add(randomImage);
        }
        return randomImages;
    }
}
