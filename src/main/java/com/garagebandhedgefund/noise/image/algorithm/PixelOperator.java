package com.garagebandhedgefund.noise.image.algorithm;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

/**
 * Created by Matt on 4/2/2016.
 */
public class PixelOperator {

    public static int medianPixel(int[] pixels) {
        Arrays.sort(pixels);
        int median;
        if (pixels.length % 2 == 0) {
            median = (pixels[pixels.length / 2] + pixels[pixels.length / 2 - 1]) / 2;
        } else {
            median = pixels[pixels.length / 2];
        }
        return median;
    }

    /**
     * This method sets a pixel value based on a percentage change.  The values can be stretched so that
     * values in a certain percentage range are represented by a larger portion of the pixel depth.  For example,
     * stock prices typically range in the +- 0 to 5% range.  In absolute terms most changes in stocks would barely
     * register as pixel changes unless that range was stretched to cover more than 5% of the pixel depth range.
     * in this case 0-5% would be stretched to cover 50% of the pixel range while the remaining percent change would fill
     * the remaining 50%
     *
     * @param percentChange Percent change of the value that will receive a pixel value. For example a stock price that changes 2%
     * @param pixelDepth The total depth of the pixel range.  For example a range of 0-255 would have a pixelDepth of '256'
     * @param maxStretchPercent The maximum percentage value to use for stretching the values. For example, if you want to stretch values from 0-5% you would use '0.05' as this value
     * @param stretchToPercentDepth The percentage of the pixel depth to use for the stetching. For example, if you want 0-5% of the values to cover 50% of the pixel depth, you would use 50% here '0.5'.
     * @return Returns a pixel value based on a percentage of change.
     */
    public static int pixelByPercentChange(double percentChange, int pixelDepth, double maxStretchPercent,
                                           double stretchToPercentDepth) {

        // Calculate the stretched %
        // For example if the maxStretchPercentage is 5% and pixel depth is 256 with a stretchToPercentDepth of 50%
        //
        // 1. Split the pixel depth in half for positive and negative values
        // 256/2 = 128
        //
        // 2. Take the remaining pixel depth and split it again based on the stretchToPercentDepth
        // 128 * .05 = 64
        //
        // 3. Find the percentage of the percentChange in comparison to the maxStretchPercent.  Assume 3% for percentChange
        // 0.03 / 0.05 = 0.6
        //
        // 4. Apply this percentage to Step #2 above rounding up
        // 64 * .6 = 38.4  round to 38
        //
        // 5. Since the percentChange was positive, add the value from Step #4 to Step #1's value.
        // 128 + 38 = 166
        //
        // The pixel value is then 166

        int pixelDepthMidPoint = (pixelDepth/2) - 1; // Minus 1 to start with zero.
        int returnPixel = pixelDepthMidPoint; // Default the return value to the midPoint (no price change)
        int stretchedPixelSize = (int) (pixelDepthMidPoint * stretchToPercentDepth);  // Stretched pixel range
        int nonStretchedPixelSize = pixelDepthMidPoint - stretchedPixelSize; // non-stretched pixel range

        if (Math.abs(percentChange) <= maxStretchPercent) {

            double percentChangeStretch = Math.abs(percentChange) / maxStretchPercent;
            int stretchedPixelValue = (int) (stretchedPixelSize * percentChangeStretch);
            if (percentChange > 0) {
                returnPixel = pixelDepthMidPoint + stretchedPixelValue;
            } else if (percentChange < 0) {
                returnPixel = pixelDepthMidPoint - stretchedPixelValue;
            }
        }

        // Calculate the remaining percentages to a pixel value.  Since the percentage could go to infinity, use an
        // asymptote function to create a limit
        // ( For example, if the percentage can't exceed a pixel depth of 64, an equation might look like:
        //  limit - (limit/percent)   where percent would be a whole number ( 1 instead of 0.01 for 1%)
        // an example would be 64 - (64/2)   where 64 is the max bit depth and 2 is 2% price change.
        if (Math.abs(percentChange) > maxStretchPercent) {
            BigDecimal adjustedPercentChange = new BigDecimal(Math.abs(percentChange)).subtract(new BigDecimal(maxStretchPercent));
            adjustedPercentChange = adjustedPercentChange.setScale(2, RoundingMode.HALF_UP);
            int percentWholeNumber = (int) (adjustedPercentChange.doubleValue() * 100);  // Convert the percent into an integer
            if (percentWholeNumber == 0) percentWholeNumber = 1;  // Avoid divide by zero errors
            int pixelValue = nonStretchedPixelSize - (nonStretchedPixelSize / percentWholeNumber); // asymptote function
            if (percentChange > 0) {
                returnPixel = pixelDepthMidPoint + stretchedPixelSize + pixelValue + 1;
            } else if (percentChange < 0) {
                returnPixel = pixelDepthMidPoint - stretchedPixelSize - pixelValue - 1;
            }
        }

        return returnPixel;
    }
}
