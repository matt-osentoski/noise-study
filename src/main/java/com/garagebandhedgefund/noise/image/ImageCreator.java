package com.garagebandhedgefund.noise.image;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

/**
 * Interface for creating images
 * Created by Matt on 5/1/2016.
 */
public interface ImageCreator {

    /**
     * Create a number of images at a specific file location
     * @param fileLocation Directory where images will be created
     * @param height Image height
     * @param width Image width
     * @return
     */
    List<BufferedImage> createImages(String fileLocation, int width, int height) throws IOException;

}
