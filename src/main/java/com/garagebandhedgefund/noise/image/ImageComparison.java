package com.garagebandhedgefund.noise.image;

import com.garagebandhedgefund.noise.exception.ImageComparisonException;
import com.garagebandhedgefund.noise.image.algorithm.PixelOperator;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.List;

/**
 * Created by Matt on 4/1/2016.
 */
@Component
public class ImageComparison {

    /**
     *
     * @param sourceImages
     */
    public BufferedImage createMedianBlendedImage(List<BufferedImage> sourceImages) throws ImageComparisonException {

        if (!hasSameDimensionImages(sourceImages)) {
            throw new ImageComparisonException("All the image files must be the same size");
        }

        BufferedImage medianImage = null;
        WritableRaster medianRaster = null;
        int width = 0;
        int height = 0;
        if (sourceImages.size() > 0) {
            BufferedImage firstSampleImg = sourceImages.get(0);
            width = firstSampleImg.getWidth();
            height = firstSampleImg.getHeight();
            medianImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
            medianRaster = medianImage.getRaster();
        }

        // For each pixel, find the median value between all the frames and add to the median image
        for(int y=0;y<height;y++) {
            for (int x = 0; x < width; x++) {
                // Loop through all the frames pushing the pixel into a pixel array for median processing.
                int[] pixelArr = new int[sourceImages.size()];
                for(int i=0; i<sourceImages.size(); i++) {
                    int pixel = sourceImages.get(i).getRaster().getSample(x,y,0);
                    pixelArr[i] = pixel;
                }
                int medianPixel = PixelOperator.medianPixel(pixelArr);
                medianRaster.setSample(x,y,0,medianPixel);
            }
        }
        return medianImage;
    }

    /**
     * Check a list of files to see if they are images and have the same dimensions.
     * @param sourceImages List of image files
     * @return true/false if all the files are images and the same dimensions.
     */
    public static boolean hasSameDimensionImages(List<BufferedImage> sourceImages) {

        BufferedImage lastImageFile = null;
        for(int x =0; x<sourceImages.size(); x++) {
            BufferedImage bi = sourceImages.get(x);

            if (x==0) {
                lastImageFile = bi;
                continue;
            } else if (bi.getWidth() != lastImageFile.getWidth() || bi.getHeight() != lastImageFile.getHeight()) {
                return false;
            }
            lastImageFile = bi;
        }
        return true;
    }
}
