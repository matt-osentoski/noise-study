package com.garagebandhedgefund.noise;

import com.garagebandhedgefund.noise.image.ImageComparison;
import com.garagebandhedgefund.noise.image.ImageCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Matt on 3/31/2016.
 */

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.garagebandhedgefund")
@EntityScan(basePackages = "com.garagebandhedgefund.trading.model")
public class Application implements CommandLineRunner {

    @Autowired
    ImageComparison imageComparison;

    @Autowired
    ImageCreator randomImageCreatorImpl;

    @Autowired
    Function<String, ImageCreator> stockPriceImageCreatorFactory;

    @Value("${stock.price.image.start.date}")
    String stockPriceStartDate;

    @Value("${image.width}")
    int width;

    @Value("${image.height}")
    int height;

    @Value("${image.directory.gauss.noise}")
    String gaussFilepath;

    @Value("${image.directory.stock.prices}")
    String stockPriceFilepath;

    @Value("${image.directory.median.blended}")
    String medianFilepath;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

        // Random Images
        new File(gaussFilepath).mkdirs();
        List<BufferedImage> randomImages = randomImageCreatorImpl.createImages(gaussFilepath, width, height);
        BufferedImage medianBlendedImage = imageComparison.createMedianBlendedImage(randomImages);
        new File(medianFilepath).mkdirs();
        File medianFile = new File(medianFilepath + "/median-file.png");
        ImageIO.write(medianBlendedImage, "png", medianFile);

        // Stock Images
        new File(stockPriceFilepath).mkdir();
        ImageCreator stockPriceImageCreatorImpl = stockPriceImageCreatorFactory.apply(stockPriceStartDate);
        List<BufferedImage> stockPriceImages =
                stockPriceImageCreatorImpl.createImages(stockPriceFilepath, width, height);
        BufferedImage medianBlendedStockImage = imageComparison.createMedianBlendedImage(stockPriceImages);
        File medianStockFile = new File(medianFilepath + "/median-stock-file.png");
        ImageIO.write(medianBlendedStockImage, "png", medianStockFile);

    }

}
