DROP PROCEDURE IF EXISTS StockToImageMapProc;
DELIMITER $$
CREATE PROCEDURE `StockToImageMapProc`()
BEGIN
  DECLARE done INT DEFAULT 0;
  DECLARE symbol_val VARCHAR(255);
  DECLARE sector_val VARCHAR(255);
  DECLARE industry_val VARCHAR(255);

  DECLARE widthVal INT DEFAULT 25;
  DECLARE heightVal INT DEFAULT 20;

  DECLARE xVal INT DEFAULT 0;
  DECLARE yVal INT DEFAULT 0;

  -- Stock cursor
  DECLARE stock_cursor CURSOR FOR SELECT symbol, sector, industry FROM stocks;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	-- Create a temp table to store results
	DROP TEMPORARY TABLE IF EXISTS tbl_results;
	CREATE TEMPORARY TABLE IF NOT EXISTS tbl_results  (
		symbol varchar(255),
    x int,
    y int,
    sector varchar(255),
    industry varchar(255)
	);

	OPEN stock_cursor;

	REPEAT
		FETCH stock_cursor INTO symbol_val, sector_val, industry_val;
      IF xVal = 25 THEN
        SET xVal = 0;
        SET yVal = yVal + 1;
      END IF;
      IF NOT done THEN
        INSERT INTO tbl_results VALUES (symbol_val, xVal, yVal, sector_val, industry_val);
        SET xVal = xVal + 1;
		  END IF;
	UNTIL done END REPEAT;

SELECT * from tbl_results;

END$$
